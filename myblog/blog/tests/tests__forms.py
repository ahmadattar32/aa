from django.test import TestCase
from blog.forms import CommentForm


class TestForms(TestCase):

    def test_Comment_form_valid_data(self):
        form = CommentForm(data={
            'author': 'Mouad717',
            'text': 'the most effective'
        })
        self.assertTrue(form.is_valid())

    def test_Comment_form_no_data(self):
        form = CommentForm(data={})
        self.assertFalse(form.is_valid())
        self.assertEquals(len(form.errors), 1)
