from django.shortcuts import render, redirect, get_object_or_404
from django.core.paginator import Paginator
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.views.generic import (
    ListView,
    DetailView,
    CreateView,
    UpdateView,
    DeleteView
)
from .models import Post, Event, Comment
from .forms import CommentForm
from django.contrib.auth.decorators import login_required
from django.contrib import messages

def home(request):
    context = {
        'Sports': Post.objects.filter(tag='Sport'),
        'Landmarks': Post.objects.filter(tag='Historical Landmarks'),
        'Restaurants': Post.objects.filter(tag='Restaurants'),
        'Divers': Post.objects.filter(tag='Divers')

    }
    return render(request, 'blog/home.html', context)

class PostListView(ListView):
    model = Post
    template_name = 'blog/home.html'
    context_object_name = 'posts'
    ordering = ['-date_posted']

class PostDetailView(DetailView):
    model = Post


class PostCreateView(LoginRequiredMixin, CreateView):
    model = Post
    fields = ['title', 'tag', 'content', 'image']

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)


class PostUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = Post
    fields = ['title', 'tag', 'content', 'image']

    def form_valid(self, form):

        return super().form_valid(form)

    def test_func(self):
        post = self.get_object()
        if self.request.user == post.author or self.request.user.profile.Class == 'Administrator':
            return True
        return False


class PostDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    model = Post
    success_url = '/'

    def test_func(self):
        post = self.get_object()
        if self.request.user == post.author or self.request.user.profile.Class == 'Administrator':
            return True
        return False

def about(request):
    return render(request, 'blog/about.html')

def archive(request):
    items = Post.objects.all().order_by("date_posted")
    p0 = Paginator(items, 10)
    page = request.GET.get('page')
    posts = p0.get_page(page)
    return render(request, 'blog/ARCHIVE.html', {'posts': posts})

def descendind(request):
    items = Post.objects.all()
    p5 = Paginator(items, 10)
    page = request.GET.get('page')
    posts = p5.get_page(page)
    return render(request, 'blog/ARCHIVE.html', {'posts': posts})

def Sport(request):
    items = Post.objects.all().filter(tag='Sport')
    p1 = Paginator(items, 10)
    page = request.GET.get('page')
    posts = p1.get_page(page)
    return render(request, 'blog/Sport.html', {'posts': posts})

def Restaurants(request):
    items = Post.objects.all().filter(tag='Restaurants')
    p2 = Paginator(items, 10)
    page = request.GET.get('page')
    posts = p2.get_page(page)
    return render(request, 'blog/Restaurants.html', {'posts': posts})

def Landmarks(request):
    items = Post.objects.all().filter(tag='Historical Landmarks')
    p3 = Paginator(items, 10)
    page = request.GET.get('page')
    posts = p3.get_page(page)
    return render(request, 'blog/Landmarks.html', {'posts': posts})

def Divers(request):
    items = Post.objects.all().filter(tag='Divers')
    p4 = Paginator(items, 10)
    page = request.GET.get('page')
    posts = p4.get_page(page)
    return render(request, 'blog/Divers.html', {'posts': posts})



class PostListViewcal(ListView):
    model = Post
    template_name = 'blog/Calender.html'
    context_object_name = 'posts'
    ordering = ['-date_posted']

def calender(request):
    items = Event.objects.all().order_by("start_time")
    p6 = Paginator(items, 10)
    page = request.GET.get('page')
    events = p6.get_page(page)
    return render(request, 'blog/Calender.html', {'events': events})


class EventDetailView(DetailView):
    model = Event


class EventCreateView(LoginRequiredMixin, CreateView):
    model = Event
    fields = ['title', 'location', 'description', 'start_time', 'end_time']

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)


class EventUpdateView(LoginRequiredMixin,  UpdateView):
    model = Event
    fields = ['title', 'location', 'description', 'start_time', 'end_time']




class EventDeleteView(LoginRequiredMixin, DeleteView):
    model = Event
    success_url = '/'



def add_comment_to_post(request, pk):
    post = get_object_or_404(Post, pk=pk)
    if request.method == "POST":
        form = CommentForm(request.POST)
        if form.is_valid():
            comment = form.save(commit=False)
            comment.post = post
            comment.author = request.user.username
            if request.user.is_authenticated:
                messages.success(request, f'Comment created !')
                comment.approve()
            else:
                messages.success(request, f'Comment created, once approved it will be publicly visible!')
            comment.save()
            return redirect('/')
    else:
        form = CommentForm()
    return render(request, 'blog/add_comment_to_post.html', {'form': form})


@login_required
def comment_approve(request, id, pk):
    post = get_object_or_404(Post, id=id)
    comment = get_object_or_404(Comment, pk=pk)
    comment.approve()
    return redirect('/')


class CommentDeleteView(DeleteView):

    model = Comment
    success_url = '/'

    def test_func(self):
        comment = self.get_object()
        if self.request.user == comment.author or self.request.user.profile.Class == 'Administrator':
            return True
        return False


def check(request):
    if request.user.is_authenticated:
        return redirect('/my_account')
    else:
        return redirect('/login')