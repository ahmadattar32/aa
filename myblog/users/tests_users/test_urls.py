from django.test import TestCase
from django.urls import reverse, resolve
from users.views import profile, my_account, register

class TestUrls(TestCase):


    def test_profile_url_resolves(self):
        url = reverse('profile')
        found = resolve(url)
        self.assertEqual(found.func, profile)


    def test_my_account_url_resolves(self):
        url = reverse('my_account')
        found = resolve(url)
        self.assertEquals(found.func, my_account)


    def test_register_url_resolves(self):
         url = reverse('register')
         found = resolve(url)
         self.assertEquals(found.func, register)